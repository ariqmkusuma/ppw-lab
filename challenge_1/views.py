from django.shortcuts import render

# Create your views here.
name = ['Muhammad Azhar Rais Zulkarnain', 'Muhammad Naufal Raihansyah']
npm = [1706074865, 1706043600]
age = [20, 18]

def index(request):
    response = {'name1': name[0], 'name2': name[1], 'npm1': npm[0], 'npm2': npm[1], 'age1': age[0],'age2': age[1]}
    return render(request, 'index_edit.html', response)
