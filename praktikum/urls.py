"""Lab1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

"""
The guide doesn't really "work" anymore since the syntax is already updated to 2.1,
refer to either the Django docs or the slides to add more sites.

P.S.: I don't really know if these are backwards compatible, so I may be screwed.
"""
from django.urls import include, path
from django.contrib import admin

urlpatterns = [
    path('admin/', admin.site.urls),
	path('lab_1/', include('lab_1.urls')),
	path('challenge_1/', include('challenge_1.urls')),
	path('sch_manager/', include('sch_manager.urls')),
	path('', include('main_site.urls')),
]
