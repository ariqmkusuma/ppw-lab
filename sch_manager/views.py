from django.shortcuts import render
from .forms import ScheduleForm
from .models import Schedule
# Create your views here.

def index(request):
	
	form = ScheduleForm(request.POST)
	manager = Schedule.objects
	schDisplay = manager.all()
	
	if form.is_valid():
		nameIn = form.cleaned_data['schName']
		dateIn = form.cleaned_data['date']
		placeIn = form.cleaned_data['place']
		catIn = form.cleaned_data['category']
			
		sch = manager.create(schName = nameIn, date = dateIn, place = placeIn, category = catIn)
		sch.save()
	
	response = {'form' : form, 'schedules' : schDisplay}
	return render(request, 'sch_manager.html', {'form' : form})
