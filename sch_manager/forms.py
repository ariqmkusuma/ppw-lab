from django import forms
import datetime

class ScheduleForm(forms.Form):
	schName = forms.CharField(label="Schedule Name", max_length=120)
	date = forms.DateField(label="Date", widget=forms.SelectDateWidget, initial=datetime.date.today)
	place = forms.CharField(label="Place", max_length=120)
	category = forms.CharField(label="Category", max_length=50)

