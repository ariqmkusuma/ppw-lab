from django.apps import AppConfig


class SchManagerConfig(AppConfig):
    name = 'sch_manager'
