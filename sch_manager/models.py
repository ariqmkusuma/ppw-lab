from django.db import models

# Create your models here.

class Schedule(models.Model):
	schName = models.CharField(default='', max_length=120)
	date = models.DateField()
	place = models.CharField(default='', max_length=120)
	category = models.CharField(null=True, max_length=50)
